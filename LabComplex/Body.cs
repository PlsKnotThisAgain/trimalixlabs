﻿using System.Collections.Generic;
using System.Linq;

namespace TrimalixLabs.LabComplex
{
	public class Body
	{
		public List<BodyPart> Parts;

		public void ChangeSex(bool toFemale)
		{
			if (toFemale)
			{
				Parts.RemoveMaleBits().AddFemaleBits();
			}
			else
			{
				Parts.RemoveFemaleBits().AddMaleBits();
			}
		}
		private List<BodyPart> GenerateSexlessBody()
		{
			return new List<BodyPart>()
			{
				new BodyPart
				{
					Name = "Head",
					HasHole = false
				},
				new BodyPart
				{
					Name = "Mouth",
					HasHole = true
				},
				new BodyPart
				{
					Name = "Nose",
					HasHole = false
				},
				new BodyPart
				{
					Name = "Left ear",
					HasHole = false
				},
				new BodyPart
				{
					Name = "Right ear",
					HasHole = false
				},
				new BodyPart
				{
					Name = "Neck",
					HasHole = false
				},
				new BodyPart
				{
					Name = "Torso",
					HasHole = false
				},
				new BodyPart
				{
					Name = "Crotch",
					HasHole = false
				},
				new BodyPart
				{
					Name = "Left breast",
					HasHole = false
				},
				new BodyPart
				{
					Name = "Right breast",
					HasHole = false
				},
				new BodyPart
				{
					Name = "Left nipple",
					HasHole = false
				},
				new BodyPart
				{
					Name = "Right nipple",
					HasHole = false
				},
				new BodyPart
				{
					Name = "Left hand",
					HasHole = false
				},
				new BodyPart
				{
					Name = "Right hand",
					HasHole = false
				},
				new BodyPart
				{
					Name = "Left arm",
					HasHole = false
				},
				new BodyPart
				{
					Name = "Right arm",
					HasHole = false
				},
				new BodyPart
				{
					Name = "Left forearm",
					HasHole = false
				},
				new BodyPart
				{
					Name = "Right forearm",
					HasHole = false
				},
				new BodyPart
				{
					Name = "Fingers",
					HasHole = false
				},
				new BodyPart
				{
					Name = "Legs",
					HasHole = false
				},
				new BodyPart
				{
					Name = "Left thigh",
					HasHole = false
				},
				new BodyPart
				{
					Name = "Right thigh",
					HasHole = false
				},
				new BodyPart
				{
					Name = "Left foot",
					HasHole = false
				},
				new BodyPart
				{
					Name = "Right foot",
					HasHole = false
				},
				new BodyPart
				{
					Name = "Ass",
					HasHole = false
				},
				new BodyPart
				{
					Name = "Anus",
					HasHole = true
				},
			};
		}

		public Body(bool female = false)
		{
			Parts = GenerateSexlessBody();
			if (female)
			{
				Parts.AddFemaleBits();
			}
			else
			{
				Parts.AddMaleBits();
			}
		}
	}

	public static class BodyExtensions
	{
		public static List<BodyPart> AddFemaleBits(this List<BodyPart> parts)
		{
			parts.AddRange(new[]
				{
					new BodyPart
					{
						Name = "Clitoris",
						HasHole = false
					},
					new BodyPart
					{
						Name = "Labia",
						HasHole = false
					},
					new BodyPart
					{
						Name = "Vagina",
						HasHole = true
					}
				}
			);
			return parts;
		}
		public static List<BodyPart> RemoveFemaleBits(this List<BodyPart> parts)
		{
			parts.RemoveAll(x => new[] { "Clitoris", "Labia", "Vagina" }.Any(y => y == x.Name));
			return parts;
		}
		public static List<BodyPart> AddMaleBits(this List<BodyPart> parts)
		{
			parts.AddRange(new[]
				{
					new BodyPart
					{
						Name = "Penis",
						HasHole = false
					},
					new BodyPart
					{
						Name = "Glans",
						HasHole = false
					},
					new BodyPart
					{
						Name = "Foreskin",
						HasHole = false
					},
					new BodyPart
					{
						Name = "Balls",
						HasHole = false
					},
				}
			);
			return parts;
		}
		public static List<BodyPart> RemoveMaleBits(this List<BodyPart> parts)
		{
			parts.RemoveAll(x => new[] { "Penis", "Glans", "Foreskin", "Balls" }.Any(y => y == x.Name));
			return parts;
		}
	}
}