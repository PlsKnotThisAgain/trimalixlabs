﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Documents;

namespace TrimalixLabs.LabComplex
{
	/// <summary>
	/// Series of tests
	/// </summary>
	[Serializable]
	public class Experiment
	{
		public List<Test> Tests = new List<Test>();
		public List<Kink> Kinks()
		{
			return Tests.SelectMany(x => x.Task.Kinks).Distinct().ToList();
		}
	}
}
