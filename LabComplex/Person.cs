﻿using System;
using System.Collections.Generic;

namespace TrimalixLabs.LabComplex
{
	[Serializable]
	public class Person
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Age { get; set; }

		public List<Item> Inventory { get; set; } = new List<Item>();
		public Body Body { get; set; }
	}
}