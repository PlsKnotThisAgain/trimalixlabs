﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TrimalixLabs.LabComplex
{
	[Serializable]
	public class BodyPart
	{
		public string Name;
		public bool HasHole = false;

		protected bool Equals(BodyPart other)
		{
			return Name.ToLower() == other.Name.ToLower();
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return Equals((BodyPart)obj);
		}

		public override int GetHashCode()
		{
			return Name.ToLower().GetHashCode();
		}

		public static bool operator ==(BodyPart left, BodyPart right)
		{
			return Equals(left, right);
		}

		public static bool operator !=(BodyPart left, BodyPart right)
		{
			return !Equals(left, right);
		}

		public bool IsHoleAvailable(Kink holeStats)
		{
			//TODO: BigHoleStats should allow double penetration
			return HasHole && !Equipped.Any(x => x.NeedsAvailableHole);
		}

		public List<IEquippable> Equipped { get; set; } = new List<IEquippable>();
	}
}