﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrimalixLabs.LabComplex
{
	[Serializable]
	public class LabComplex
	{
		public List<Scientist> Scientists;
		public List<Subject> Subjects;
		public List<Subject> BgSubjects;
		public Experiment Experiment;
	}
}
