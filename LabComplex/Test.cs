﻿using System;
using System.Collections.Generic;

namespace TrimalixLabs.LabComplex
{
	/// <summary>
	/// A single test with a task for one or more subjects.
	/// </summary>
	[Serializable]
	public class Test
	{
		public TestTask Task { get; set; }

	}
	[Serializable]
	public class TestTask
	{
		public string Name { get; set; }
		public string Description { get; set; }
		public int Level { get; set; }
		public List<Kink> Kinks { get; set; }
	}
}
