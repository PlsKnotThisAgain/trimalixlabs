﻿using System;
using System.Collections.Generic;

namespace TrimalixLabs.LabComplex
{
	[Serializable]
	public class SubjectStats
	{
		public int TimesSucceeded;
		public int TimesFailed;
		public int TestCount => TimesSucceeded + TimesFailed;
		public List<Kink> Kinks;
	}
}