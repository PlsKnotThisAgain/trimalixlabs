﻿using System;

namespace TrimalixLabs.LabComplex
{
	[Serializable]
	public class Kink
	{
		public string Name { get; set; }
		public string Description { get; set; }
		public int Weight { get; set; }

		protected bool Equals(Kink other)
		{
			return Name.ToLower() == other.Name.ToLower();
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return Equals((Kink)obj);
		}

		public override int GetHashCode()
		{
			return Name != null ? Name.ToLower().GetHashCode() : 0;
		}

		public static bool operator ==(Kink left, Kink right)
		{
			return Equals(left, right);
		}

		public static bool operator !=(Kink left, Kink right)
		{
			return !Equals(left, right);
		}
	}
}