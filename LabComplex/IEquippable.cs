﻿namespace TrimalixLabs.LabComplex
{
	public interface IEquippable
	{
		bool NeedsAvailableHole { get; set; }
		string BodyPart { get; set; }
	}
}