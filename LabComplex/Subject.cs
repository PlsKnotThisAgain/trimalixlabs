﻿using System.Net.Sockets;

namespace TrimalixLabs.LabComplex
{
	public class Subject : Person
	{
		public string SubjectName { get; set; }
		public string Profession { get; set; }
		public Address Address { get; set; } = new Address();
		public SubjectStats Stats { get; set; }
	}
}