﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrimalixLabs.LabComplex;

namespace TrimalixLabs.Misc
{
	[Serializable]
	public class TestPackage
	{
		public string Name { get; set; } = "Package name";
		public string Description { get; set; } = "Package description";
		public int Version { get; set; } = 0;
		public ObservableCollection<TestTask> Tests { get; set; } = new ObservableCollection<TestTask>();
	}
}
