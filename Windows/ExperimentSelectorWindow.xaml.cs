﻿using System.Windows;
using TrimalixLabs.LabComplex;
using TrimalixLabs.Misc;

namespace TrimalixLabs.Windows
{
	/// <summary>
	/// Interaction logic for ExperimentSelectorWindow.xaml
	/// </summary>
	public partial class ExperimentSelectorWindow
	{
		public LabComplex.LabComplex LabComplex;
		public ExperimentSelectorWindow()
		{
			InitializeComponent();
			LabComplex = new LabComplex.LabComplex();
		}

		private void OnCancelSetup(object sender, RoutedEventArgs e)
		{
			WindowManager.Instance.ChangeTo<LauncherWindow>(this);
		}
	}
}
