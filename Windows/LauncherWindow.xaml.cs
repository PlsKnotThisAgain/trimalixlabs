﻿using System.Diagnostics;
using System.Globalization;
using System.Speech.Synthesis;
using System.Windows;
using TrimalixLabs.Misc;

namespace TrimalixLabs.Windows
{
	/// <summary>
	/// Interaction logic for LauncherWindow.xaml
	/// </summary>
	public partial class LauncherWindow
	{
		public LauncherWindow()
		{
			InitializeComponent();
		}

		private void OnEditPackage(object sender, RoutedEventArgs e)
		{
			WindowManager.Instance.ChangeTo<TestPackageEditorWindow>(this);
		}
		private void OnBeginExperiment(object sender, RoutedEventArgs e)
		{
			WindowManager.Instance.ChangeTo<ExperimentSelectorWindow>(this);
			return;
			Debug.Write("Starting an experiment");
			SpeechSynthesizer synth = new SpeechSynthesizer();
			synth.Rate = 1;

			synth.SetOutputToDefaultAudioDevice();
			PromptBuilder p = new PromptBuilder();
			p.Culture = CultureInfo.CreateSpecificCulture("en-US");
			p.StartVoice(VoiceGender.Female);
			p.StartSentence();
			p.AppendAudio("uas.wav");
			p.AppendText("Your order for ");
			p.StartStyle(new PromptStyle() { Emphasis = PromptEmphasis.Strong, Rate = PromptRate.Slow });
			p.AppendText("8 dicks");
			p.EndStyle();
			p.AppendText(" will be shipped into Your butthole tomorrow.");
			p.EndSentence();
			p.EndVoice();

			synth.SelectVoiceByHints(VoiceGender.Female);
			synth.SpeakAsync(p);
			System.Media.SoundPlayer player = new System.Media.SoundPlayer(Properties.Resources.uas);
			player.Play();
		}
	}
}
