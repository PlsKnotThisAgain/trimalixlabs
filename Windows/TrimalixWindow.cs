﻿using System.Windows;
using System.Windows.Input;
using TrimalixLabs.Misc;

namespace TrimalixLabs.Windows
{
	/// <summary>
	/// Interaction logic for TrimalixWindow.xaml
	/// </summary>
	public class TrimalixWindow : Window
	{
		protected void OnClickExit(object sender, RoutedEventArgs e)
		{
			WindowManager.Instance.Exit();
		}
		protected void OnStatusBarDown(object sender, MouseButtonEventArgs e)
		{
			if (e.ChangedButton == MouseButton.Left)
				this.DragMove();
		}
	}
}
