﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Newtonsoft.Json;
using TrimalixLabs.LabComplex;
using TrimalixLabs.Misc;
using Button = System.Windows.Controls.Button;

namespace TrimalixLabs.Windows
{
	/// <summary>
	/// Interaction logic for TestPackageEditorWindow.xaml
	/// </summary>
	public partial class TestPackageEditorWindow
	{

		public TestPackage Package { get; set; } = new TestPackage();
		public string SelectedFile { get; set; }

		public ICommand DeleteItemCommand { get; set; }

		public TestPackageEditorWindow()
		{
			InitializeComponent();
			DeleteItemCommand = new ActionCommand(x => Package.Tests.Remove((TestTask)x));
		}
		private void DeleteItem(object item, RoutedEventArgs e)
		{
			Button a = (Button)item;

			Debug.WriteLine(e);
		}
		private void OnOpenFile(object sender, RoutedEventArgs e)
		{
			throw new NotImplementedException();
		}

		private void OnCancel(object sender, RoutedEventArgs e)
		{
			WindowManager.Instance.ChangeTo<LauncherWindow>(this);
		}

		private void OnSaveFile(object sender, RoutedEventArgs e)
		{
			if (SelectedFile == null)
			{
				var dialog = new Microsoft.Win32.SaveFileDialog();
				dialog.AddExtension = true;
				dialog.DefaultExt = ".json";
				dialog.Filter = "JSON File|.json";
				var result = dialog.ShowDialog();
				if (result.HasValue && result.Value)
				{
					SelectedFile = dialog.FileName;
				}
			}

			if (SelectedFile != null)
			{
				File.WriteAllText(SelectedFile, JsonConvert.SerializeObject(Package, Formatting.Indented));
			}
		}

		private void OnAddNewTest(object sender, RoutedEventArgs e)
		{
			Package.Tests.Add(new TestTask());
		}

	}
}
