# TrimalixLabs

### A kidnapping-testing-laboratoring-breaking simulation

Do *You* or Your significant other sometimes think: "Wouldn't it be awesome, if some company just kidnapped me, took me to their laboratories, and performed lewd experiments on me?"

**Of course yes!**

We at Trimalix Laboratories aim to make those dreams closer to reality (or at least RP-eality).

## So wtf is this?

Great! You're still reading, so You're apparently at least a bit curious. This is a free program, currently only for Windows that tries to create some immersive environment for roleplaying.

### Is it good?

I don't know, I'm not that far with development. Maybe it's gonna be completely horrible, who knows!

### So what it do?

Not much is in yet, but planned features are:

* Audio system with a TTS announcer that will comment everything that is happening
  * Audio effects on the TTS so it sounds more like an announcer would (VST. I'm thinking a vocoder, reverb, possibly custom ones)
  * BG/Immersion messages about other subjects
* Laboratory complex system, where You can set up available rooms
* Tests for the subjects with instructions for the scientists
  * Hopefully with a lot of kinks. Every kink will be disableable though.
* Experiments - Collections of tests
  * Random ones - just get new and new tasks for a set amount of time
  * Presets - Manually crafted series of tests that can follow similar theme or a story
* Everything is saveable to json, so You can create and share experiments and collections of tasks

I'd love to add some kind of a progression, where the tasks get more difficult over time.
I'm not gonna bother with detailed commit messages until I get the basic structure in.