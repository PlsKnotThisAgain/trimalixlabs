﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using TrimalixLabs.Windows;

namespace TrimalixLabs.Misc
{
	class WindowManager
	{
		private static WindowManager _instance;

		public static WindowManager Instance => _instance ?? (_instance = new WindowManager());

		private Dictionary<Type, TrimalixWindow> windows = new Dictionary<Type, TrimalixWindow>();

		public void Exit()
		{
			Application.Current.Shutdown();
		}

		private WindowManager() { }

		public void ChangeTo<T>(TrimalixWindow trimalixWindow) where T : TrimalixWindow, new()
		{
			Debug.Assert(typeof(T).IsSubclassOf(typeof(TrimalixWindow)), "[ERROR] You can only change to a TrimalixWindow You dense bitch!");
			if (!windows.ContainsKey(typeof(T)))
			{
				windows.Add(typeof(T), new T());
			}
			trimalixWindow.Hide();
			windows[typeof(T)].Width = trimalixWindow.Width;
			windows[typeof(T)].Height = trimalixWindow.Height;
			windows[typeof(T)].Top = trimalixWindow.Top;
			windows[typeof(T)].Left = trimalixWindow.Left;
			windows[typeof(T)].Show();
		}
	}
}
