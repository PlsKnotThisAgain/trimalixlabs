﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using TrimalixLabs.LabComplex;

namespace TrimalixLabs.Managers
{
	[Serializable]
	class DatabaseManager
	{
		private static DatabaseManager _instance;
		public static DatabaseManager Instance {
			get {
				if (_instance == null)
				{
					if (File.Exists(@"database.json"))
					{
						_instance = JsonConvert.DeserializeObject<DatabaseManager>(File.ReadAllText(@"database.json"));
					}
					else
					{
						_instance = new DatabaseManager();
					}
				}
				return _instance;
			}
		}

		private DatabaseManager() { }

		public List<Experiment> Experiments = new List<Experiment>();
		public List<Test> Tests = new List<Test>();
		public List<Person> People = new List<Person>();
		public List<Scientist> Scientists = new List<Scientist>();
		public List<Subject> Subjects = new List<Subject>();

		public void Save()
		{
			File.WriteAllText(@"database.json", JsonConvert.SerializeObject(this, Formatting.Indented));
		}
	}
}
